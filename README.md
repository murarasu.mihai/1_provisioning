: [Home](https://gitlab.com/fii-practic-yonder) / [Mediul de lucru](./README.md)

# Cum pornim masinile virtuale si configuram mediul de lucru

Pentru a va usura munca am creat o metoda automata de a porni si configura mediul de lucru ce va fi folosit pentru workshopuri 

0. Descarcam si instalam un SSH Client - spre exemplu, PuTTY de [aici]( https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.73-installer.msi)

1. Descarcam si instalam Virtual Box de [aici](https://download.virtualbox.org/virtualbox/6.1.4/VirtualBox-6.1.4-136177-Win.exe)

2. Descarcam si instalam Vagrant de [aici](https://releases.hashicorp.com/vagrant/2.2.7/vagrant_2.2.7_x86_64.msi)

3. Daca nu ati ales sa nu dati restart dupa fiecare pas de instalare, dati un Reboot acum pentru a se aplica driverele.

4. Cream un folder nou, dedicat, numit `Vagrant`. In acest folder cream fisierul denumit Vagrantfile (continutul fisierului il gasiti [aici](./Vagrantfile)). Aveti mare grija sa nu existe spatii in path-ul spre fisier. Spre exemplu `D:\Downloads\Vagrant\Vagrantfile` e un path valid, dar `D:\My Documents\Vagrant\Vagrantfile` nu este. Daca salvati fisierul, asigurati-va sa nu aiba extensie, `Vagrantfile.txt` nu e valid.

5. Deschidem Command Prompt in folderul unde am descarcat fisierul Vagrantfile. Apasam pe bara de adrese in File explorer, scriem `cmd` si apasam tasta `Enter`.

6. Ne asiguram ca suntem in folderul unde e fisierul [Vagrantfile](./Vagrantfile). Scriem `dir` in fereastra Command Prompt, ar trebui sa vedem fisierul Vagrantfile. Daca nu apare, verificati ca sunteti in folderul care trebuie.

7. Executam comanda `vagrant up`

8. Lasam comanda sa ruleze. Aceasta va dura cateva minute, in functie de viteza conexiunii la internet. La final va aparea ca s-a executat cu succes.

9. Oprim masinile virtuale dupa ce s-au creat cu `vagrant halt`, urmand ca sa le pornim in functie de cum avem nevoie folosindu-ne de VirtualBox.


# Cum ne generam un set de Keys pentru a lucra cu GitLab in urmatoarele Workshop-uri

1. Pornim sau ne asiguram ca e pornita masina virtuala cu numele de `management`.

2. Folosind Putty, ne conectam la masina pe baza IP'ului din reteaua privata creata. Hostname: root@192.168.111.11 cu Parola: `fiipractic`

3. Schimbam parola intr-una mai sigura, folosind comanda passwd: ` passwd root ` si urmam instructiunile.

4. Daca nu am intrat la pasii precedenti cu userul de root, ne asiguram ca suntem root folosing comanda `whoami` sau `id`.

5. Generam cheile necesare folosind comanda `ssh-keygen -t rsa -C "<user@e-mmail>"`  e.g: (ssh-keygen -t rsa -C "mihai.murarasu@tss-yonder.com" ). Pentru optiunile ce apar pe ecran nu vom introduce nimic, vom apasa doar Enter pentru toate.

6. Afisam copia publica generata folosind comanda `cat /root/.ssh/id_rsa.pub`. Copiem outputul acestei comenzi in clipboard ( Select, in Putty ).

7. Intram pe pagina GitLab: `https://gitlab.com/profile/keys` unde vom adauga cheia publica copiata mai sus, o vom denumi FiiPractic si o vom salva.


# Time to play :) 

1. Intram cu Putty ( daca nu avem deja un Terminal deschis ) pe masina de management

2. Rulam comanda ` cd /root/learn/bashcrawl-master/entrance ; cat scroll ` 

3. Urmam instructiunile de pe ecran :D. 



* * *
![license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)
